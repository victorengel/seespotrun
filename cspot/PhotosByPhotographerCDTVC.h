//
//  PhotosByPhotographerCDTVC.h
//  cspot
//
//  Created by Victor Engel on 4/4/13.
//  Copyright (c) 2013 Victor Engel. All rights reserved.
//

#import "CoreDataTableViewController.h"
#import "Photographer.h"

@interface PhotosByPhotographerCDTVC : CoreDataTableViewController

@property (nonatomic, strong) Photographer *photographer;


@end
