//
//  FlickrPhoto+Create.h
//  cspot
//
//  Created by Victor Engel on 4/11/13.
//  Copyright (c) 2013 Victor Engel. All rights reserved.
//

#import "FlickrPhoto.h"

@interface FlickrPhoto (Create)

+(FlickrPhoto *)flickrPhotoWithPhotoID:(NSString *)photoID
                inManagedObjectContext:(NSManagedObjectContext *)context
                                 width:(NSNumber *)width
                                height:(NSNumber *)height
                                   url:(NSString *)url
                                source:(NSString *)source
                                 label:(NSString *)label;
+(FlickrPhoto *)flickrPhotoWithPhotoID:(NSString *)photoID
                inManagedObjectContext:(NSManagedObjectContext *)context
                    largestSmallerThan:(int)sizeLimit;

@end
