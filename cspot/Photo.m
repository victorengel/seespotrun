//
//  Photo.m
//  cspot
//
//  Created by Victor Engel on 4/11/13.
//  Copyright (c) 2013 Victor Engel. All rights reserved.
//

#import "Photo.h"
#import "FlickrPhoto.h"
#import "Photographer.h"
#import "Tag.h"


@implementation Photo

@dynamic dateTaken;
@dynamic dateUploaded;
@dynamic photoDescription;
@dynamic photoID;
@dynamic photoTitle;
@dynamic thumbnail;
@dynamic thumbnailURL;
@dynamic canBlog;
@dynamic canDownload;
@dynamic canPrint;
@dynamic photographer;
@dynamic tags;
@dynamic flickrPhotos;

@end
