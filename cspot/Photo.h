//
//  Photo.h
//  cspot
//
//  Created by Victor Engel on 4/11/13.
//  Copyright (c) 2013 Victor Engel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class FlickrPhoto, Photographer, Tag;

@interface Photo : NSManagedObject

@property (nonatomic, retain) NSDate * dateTaken;
@property (nonatomic, retain) NSDate * dateUploaded;
@property (nonatomic, retain) NSString * photoDescription;
@property (nonatomic, retain) NSString * photoID;
@property (nonatomic, retain) NSString * photoTitle;
@property (nonatomic, retain) NSData * thumbnail;
@property (nonatomic, retain) NSString * thumbnailURL;
@property (nonatomic, retain) NSNumber * canBlog;
@property (nonatomic, retain) NSNumber * canDownload;
@property (nonatomic, retain) NSNumber * canPrint;
@property (nonatomic, retain) Photographer *photographer;
@property (nonatomic, retain) NSSet *tags;
@property (nonatomic, retain) NSSet *flickrPhotos;
@end

@interface Photo (CoreDataGeneratedAccessors)

- (void)addTagsObject:(Tag *)value;
- (void)removeTagsObject:(Tag *)value;
- (void)addTags:(NSSet *)values;
- (void)removeTags:(NSSet *)values;

- (void)addFlickrPhotosObject:(FlickrPhoto *)value;
- (void)removeFlickrPhotosObject:(FlickrPhoto *)value;
- (void)addFlickrPhotos:(NSSet *)values;
- (void)removeFlickrPhotos:(NSSet *)values;

@end
