//
//  MyUIManagedDocument.m
//  cspot
//
//  Created by Victor Engel on 4/8/13.
//  Copyright (c) 2013 Victor Engel. All rights reserved.
//

#import "MyUIManagedDocument.h"
#import "CoreData/CoreData.h"

@implementation MyUIManagedDocument

- (id)contentsForType:(NSString *)typeName error:(NSError *__autoreleasing *)outError
{
   NSLog(@"$$$$$$$$$$$ Auto-Saving Document");
   return [super contentsForType:typeName error:outError];
}

- (void)handleError:(NSError *)error userInteractionPermitted:(BOOL)userInteractionPermitted
{
   NSLog(@"$$$$$$$$$$$ UIManagedDocument error: %@", error.localizedDescription);
   NSArray* errors = [[error userInfo] objectForKey:NSDetailedErrorsKey];
   if(errors != nil && errors.count > 0) {
      for (NSError *error in errors) {
         NSLog(@"  Error: %@", error.userInfo);
      }
   } else {
      NSLog(@"  %@", error.userInfo);
   }
}
@end
