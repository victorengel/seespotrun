//
//  PhotographerCDTVC.h
//  cspot
//
//  Created by Victor Engel on 4/3/13.
//  Copyright (c) 2013 Victor Engel. All rights reserved.
//

#import "CoreDataTableViewController.h"

@interface PhotographerCDTVC : CoreDataTableViewController

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext; // in order to see the database

@end
