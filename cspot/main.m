//
//  main.m
//  cspot
//
//  Created by Victor Engel on 4/3/13.
//  Copyright (c) 2013 Victor Engel. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "cspotAppDelegate.h"

int main(int argc, char *argv[])
{
   @autoreleasepool {
       return UIApplicationMain(argc, argv, nil, NSStringFromClass([cspotAppDelegate class]));
   }
}
