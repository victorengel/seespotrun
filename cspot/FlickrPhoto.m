//
//  FlickrPhoto.m
//  cspot
//
//  Created by Victor Engel on 4/11/13.
//  Copyright (c) 2013 Victor Engel. All rights reserved.
//

#import "FlickrPhoto.h"
#import "Photo.h"


@implementation FlickrPhoto

@dynamic url;
@dynamic photoID;
@dynamic width;
@dynamic height;
@dynamic dateCached;
@dynamic dateViewed;
@dynamic source;
@dynamic label;
@dynamic largeDimension;
@dynamic photo;

@end
