//
//  Photo+Flickr.m
//  cspot
//
//  Created by Victor Engel on 4/3/13.
//  Copyright (c) 2013 Victor Engel. All rights reserved.
//

#import "Photo+Flickr.h"
#import "FlickrFetcher.h"
#import "Photographer+Create.h"
#import "Tag+Create.h"

@implementation Photo (Flickr)

+ (Photo *)photoWithFlickrInfo:(NSDictionary *)photoDictionary
         inManagedObjectConext:(NSManagedObjectContext *)context;
{
   //NSLog(@"%@ context is %@",self,context);
   //NSLog(@"%@ photoWithFlickrInfo (probably called by DemoPhotogapherCDTVC",self);
   Photo *photo = nil;
   // Create a photo object.
   NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Photo"];
   //request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"photoTitle" ascending:YES]];
   request.predicate = [NSPredicate predicateWithFormat:@"photoID = %@", [photoDictionary[FLICKR_PHOTO_ID] description]];
   
   NSError *error = nil;
   NSArray *matches = [context executeFetchRequest:request error:&error];
   
   if (!matches || ([matches count] > 1)) {
      // handle error
   } else if (![matches count]) {
      //There were no hits, so insert a new photo object.
      photo = [NSEntityDescription insertNewObjectForEntityForName:@"Photo" inManagedObjectContext:context];
      photo.photoTitle = [photoDictionary[FLICKR_PHOTO_TITLE] description];
      photo.photoDescription = [[photoDictionary valueForKeyPath:FLICKR_PHOTO_DESCRIPTION] description];
      photo.thumbnailURL = [[FlickrFetcher urlForPhoto:photoDictionary format:FlickrPhotoFormatSquare] absoluteString];
      NSLog(@"Added thumbnailURL     %@",photo.thumbnailURL);
      
      NSString *tags = [photoDictionary valueForKey:FLICKR_TAGS];
      NSArray *tagList = [tags componentsSeparatedByString:@" "];
      NSMutableSet *tagIDs = [[NSMutableSet alloc] init];
      for (NSString *tag in tagList) {
         Tag *tagID = [Tag tagWithTag:tag inManagedObjectContext:context];
         if (tagID) {
            [tagIDs addObject:tagID];
         }
      }
      
      photo.tags = [tagIDs copy];
      photo.photoID = [photoDictionary[FLICKR_PHOTO_ID] description];
      NSLog(@"Added tags %@ for photo %@",tags,photo.photoID);
      
      NSString *photographerName = [photoDictionary[FLICKR_PHOTO_OWNER] description];
      Photographer *photographer = [Photographer photographerWithName:photographerName inManagedObjectContext:context];
      photo.photographer = photographer;
      NSLog(@"Added photographer     %@",photo.photographer);
      //NSLog(@"-----------------------");
   } else {
      // only one so set it
      photo = matches[0];
      NSLog(@"%@ found a photo having photographer named %@",self,photo.photographer.name);
      NSLog(@"Does it have a thumbnail? %@",photo.photographer.thumbnail ? @"YES" : @"NO");
   }
   return photo;
}


@end
