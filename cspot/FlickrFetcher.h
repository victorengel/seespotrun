//
//  FlickrFetcher.h
//
//  Created for Stanford CS193p Winter 2013.
//  Copyright 2013 Stanford University
//  All rights reserved.
//

#import <Foundation/Foundation.h>

#define FLICKR_PHOTO_TITLE @"title"
#define FLICKR_PHOTO_DESCRIPTION @"description._content"
#define FLICKR_PLACE_NAME @"_content"
#define FLICKR_PHOTO_ID @"id"
#define FLICKR_LATITUDE @"latitude"
#define FLICKR_LONGITUDE @"longitude"
#define FLICKR_PHOTO_OWNER @"ownername"
#define FLICKR_PHOTO_PLACE_NAME @"derived_place"
#define FLICKR_TAGS @"tags"

#define FLICKRPHOTO_WIDTH @"width"
#define FLICKRPHOTO_HEIGHT @"height"
#define FLICKRPHOTO_LABEL @"label"
#define FLICKRPHOTO_URL @"url"
#define FLICKRPHOTO_SOURCE @"source"

#define NSLOG_FLICKR NO

typedef enum {
	FlickrPhotoFormatSquare = 1,         /*  75 x   75 */
	FlickrPhotoFormatLarge = 2,
   //FlickrPhotoFormatLargeSquare = 2,    /* 150 x  150 */
   //FlickrPhotoFormatThumbnail = 3,      /* 100 x   75 */
   //FlickrPhotoFormatSmall = 4,          /* 240 x  180 */
   //FlickrPhotoFormatSmall320 = 5,       /* 320 x  240 */
   //FlickrPhotoFormatMedium = 6,         /* 500 x  375 */
   FlickrPhotoFormatMedium640 = 7,      /* 640 x  480 */
   //FlickrPhotoFormatMedium800 = 8,      /* 800 x  600 */
   //FlickrPhotoFormatLarge = 9,          /*1024 x  768 */
	FlickrPhotoFormatOriginal = 64
} FlickrPhotoFormat;

@interface FlickrFetcher : NSObject

+ (NSArray *)stanfordPhotos;

+ (NSURL *)urlForPhoto:(NSDictionary *)photo format:(FlickrPhotoFormat)format;

+ (NSArray *)latestGeoreferencedPhotos;

+ (NSArray *)availableSizesForPhoto:(id)photo_id;


@end
