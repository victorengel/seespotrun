//
//  Photographer.m
//  cspot
//
//  Created by Victor Engel on 4/11/13.
//  Copyright (c) 2013 Victor Engel. All rights reserved.
//

#import "Photographer.h"
#import "Photo.h"


@implementation Photographer

@dynamic name;
@dynamic thumbnail;
@dynamic thumbnailURL;
@dynamic photos;

@end
