//
//  IViewController.m
//  SPoT
//
//  Created by Victor Engel on 3/20/13.
//  Copyright (c) 2013 Victor Engel. All rights reserved.
//


#import "IViewController.h"

@interface IViewController () <UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;
@property (strong,nonatomic) UIImageView *imageView;
@end

@implementation IViewController

#pragma mark - UISplitViewControllerDelegate

-(void)setSplitViewBarButtonItem:(UIBarButtonItem *)barButtonItem
{
   UIToolbar *toolbar = self.toolbar;
   NSMutableArray *toolbarItems = [toolbar.items mutableCopy];
   if (_splitViewBarButtonItem)  [toolbarItems removeObject:_splitViewBarButtonItem];
   if (barButtonItem)            [toolbarItems insertObject:barButtonItem atIndex:0];
   self.toolbar.items = [toolbarItems copy];
   _splitViewBarButtonItem = barButtonItem;
}

-(void)setImageURL:(NSURL *)imageURL
{
   _imageURL = imageURL;
   [self resetImage];
}

-(NSData *)getImageDataForUrl: (NSURL *)imageURL
{
   NSData *imageData;
   if (imageURL) {
      NSFileManager *fileManager = [[NSFileManager alloc] init];
      NSArray *urls = [fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
      //Sample of urls[0]: file://localhost/var/mobile/Applications/####/Library/Document/
      NSURL *cachedURL = urls[0];
      //Manually add a cache directory.
      NSString *lastComponent = [imageURL lastPathComponent];
      cachedURL = [cachedURL URLByAppendingPathComponent:@"cache"];
      NSArray *passedPathComponents = [imageURL pathComponents];
      for (NSString *pathComponent in passedPathComponents) {
         if ([pathComponent isEqualToString:lastComponent]) {
            //Now we're looking at the file name. Ensure the directory exists. What we have so far is the directory.
            if ([fileManager createDirectoryAtURL:cachedURL withIntermediateDirectories:YES attributes:nil error:NULL]) {
               //NSLog(@"Directory was created or already exists");
            } else {
               NSLog(@"Error creating directory %@",[cachedURL description]);
            };
         }
         cachedURL = [cachedURL URLByAppendingPathComponent:pathComponent];
      }
      // Check if image data is cached.
      // If cached, load data from cache.
      imageData = [[NSData alloc] initWithContentsOfURL:cachedURL];
      if (imageData) {
         //Cached image data found
      } else  {
         // Did not find the image in cache. Retrieve it and store it.
         // Else (not cached), load data from passed imageURL.
         //     Update cache with new data.
         imageData = [[NSData alloc] initWithContentsOfURL:self.imageURL];
         if (imageData) {
            // Write the imageData to cache
            [imageData writeToURL:cachedURL atomically:YES];
         }
      }
   }
   return imageData;
}
-(void)resetImage
{
   static int networkCounter = 0;
   if (self.scrollView) {
      UIApplication *myApplication = [UIApplication sharedApplication];
      self.scrollView.contentSize = CGSizeZero;
      [self.spinner startAnimating];
      self.imageView.image = nil;
      NSURL *imageURL = self.imageURL;
      dispatch_queue_t downloadQueue = dispatch_queue_create("image downloader", NULL);
      dispatch_async(downloadQueue, ^{
         networkCounter += 1;
         myApplication.networkActivityIndicatorVisible = YES;
         NSData *imageData = [self getImageDataForUrl:self.imageURL];
         networkCounter -= 1;
         UIImage *image = [[UIImage alloc] initWithData:imageData];
         if (self.imageURL == imageURL) {
            //We know we have an image if we get here.
            dispatch_async(dispatch_get_main_queue(), ^{
               // Only do the next part if the user still expects the same image by this time.
               if (image) {
                  self.scrollView.zoomScale = 1.0;
                  self.scrollView.contentSize = image.size;
                  self.imageView.image = image;
                  self.imageView.frame = CGRectMake(0, 0, image.size.width, image.size.height);
                  [self setScale];
               }
               [self.spinner stopAnimating];
            });
         }
         dispatch_async(dispatch_get_main_queue(), ^{
            if (networkCounter == 0) myApplication.networkActivityIndicatorVisible = NO;
         });
      });
   }
}
-(void)setScale {
   if (self.imageView.image.size.width) {
      float widthScale = self.scrollView.bounds.size.width/ self.imageView.image.size.width;
      float heightScale = self.scrollView.bounds.size.height/ self.imageView.image.size.height;
      if (widthScale >= heightScale) {
         self.scrollView.zoomScale = heightScale;
      } else {
         self.scrollView.zoomScale = widthScale;
      }
   }
}
-(void)viewDidLayoutSubviews
{
   [super viewDidLayoutSubviews];
   [self setScale];
   //Following line is necessary because when the setter is called by the segue in the other controller, the titlebar does not exist.
   self.splitViewBarButtonItem = self.splitViewBarButtonItem;
   [self updateBarButtonTitle];
}
-(void)updateBarButtonTitle
{
   for (id toolbarItem in self.toolbar.items) {
      if ([toolbarItem isKindOfClass:[UIBarButtonItem class]]) {
         UIBarButtonItem *titleButton = toolbarItem;
         if (titleButton.tag == 100) {
            titleButton.title = self.title;
         }
      }
   }
}
-(UIImageView *)imageView
{
   if (!_imageView) _imageView = [[UIImageView alloc]initWithFrame:CGRectZero];
   return _imageView;
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
   return self.imageView;
}

- (void)viewDidLoad
{
   [super viewDidLoad];
   [self.scrollView addSubview:self.imageView];
   self.scrollView.minimumZoomScale = 0.2;
   self.scrollView.maximumZoomScale = 2.0;
   self.scrollView.delegate = self;
   [self resetImage];
}

@end
