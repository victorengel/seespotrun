//
//  FlickrPhoto.h
//  cspot
//
//  Created by Victor Engel on 4/11/13.
//  Copyright (c) 2013 Victor Engel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Photo;

@interface FlickrPhoto : NSManagedObject

@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) NSString * photoID;
@property (nonatomic, retain) NSNumber * width;
@property (nonatomic, retain) NSNumber * height;
@property (nonatomic, retain) NSDate * dateCached;
@property (nonatomic, retain) NSDate * dateViewed;
@property (nonatomic, retain) NSString * source;
@property (nonatomic, retain) NSString * label;
@property (nonatomic, retain) NSNumber * largeDimension;
@property (nonatomic, retain) Photo *photo;

@end
