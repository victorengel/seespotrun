//
//  PhotographerCDTVC.m
//  cspot
//
//  Created by Victor Engel on 4/3/13.
//  Copyright (c) 2013 Victor Engel. All rights reserved.
//

#import "PhotographerCDTVC.h"
#import "Photographer.h"
#import "Photo.h"

@implementation PhotographerCDTVC

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
   NSIndexPath *indexPath = nil;
   
   if ([sender isKindOfClass:[UITableViewCell class]]) {
      indexPath = [self.tableView indexPathForCell:sender];
   }
   
   if (indexPath) {
      if ([segue.identifier isEqualToString:@"setPhotographer:"]) {
         Photographer *photographer = [self.fetchedResultsController objectAtIndexPath:indexPath];
         NSLog(@"%@ photographer has thumbnail? %@",self,photographer.thumbnail ? @"YES" : @"NO");
         if ([segue.destinationViewController respondsToSelector:@selector(setPhotographer:)])
         {
            [segue.destinationViewController performSelector:@selector(setPhotographer:) withObject:photographer];
         }
      }
   }
}

-(void) setManagedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
   _managedObjectContext = managedObjectContext;
   //NSLog(@"%@ context is %@",self,managedObjectContext);
   if (managedObjectContext) {
      NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Photographer"];
      request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)]];
      request.predicate = nil; //not needed? -- all photographers
      self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:managedObjectContext sectionNameKeyPath:nil cacheName:nil];
   } else {
      // Must have a context to access the database.
      self.fetchedResultsController = nil;
   }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Photographer"];
   
   // Ask the fetchedResultsController for the photographer that corresponds to that cell.
   Photographer *photographer = [self.fetchedResultsController objectAtIndexPath:indexPath];
   NSLog(@"%@ photographer name is %@",self,photographer.name);
   NSLog(@"%@ has thumbnail? %@",self,photographer.thumbnail ? @"YES" : @"NO");
   //Now that we have the object, we can assign some of its properties.
   cell.textLabel.text = photographer.name;
   int photoCount = [photographer.photos count];
   cell.detailTextLabel.text = [NSString stringWithFormat:@"%d photos", photoCount];
   cell.imageView.image = [[UIImage alloc] initWithData:photographer.thumbnail];
   //[self.managedObjectContext save:nil];
   //NSLog(@"%@ ##### saved context in cellForRowAtIndexPath",self);
   return cell;
}
@end
