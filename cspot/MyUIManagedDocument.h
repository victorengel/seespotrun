//
//  MyUIManagedDocument.h
//  cspot
//
//  Created by Victor Engel on 4/8/13.
//  Copyright (c) 2013 Victor Engel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyUIManagedDocument : UIManagedDocument

- (id)contentsForType:(NSString *)typeName error:(NSError *__autoreleasing *)outError;


- (void)handleError:(NSError *)error userInteractionPermitted:(BOOL)userInteractionPermitted;

@end
