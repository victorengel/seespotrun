//
//  DemoPhotographerCDTVC.m
//  cspot
//
//  Created by Victor Engel on 4/3/13.
//  Copyright (c) 2013 Victor Engel. All rights reserved.
//

#import "DemoPhotographerCDTVC.h"
#import "FlickrFetcher.h"
#import "Photo+Flickr.h"
#import "MyUIManagedDocument.h"

@implementation DemoPhotographerCDTVC

-(void)viewWillAppear:(BOOL)animated
{
   [super viewWillAppear:animated];
   if (!self.managedObjectContext) {
      [self useDemoDocument];
   }
}
-(void)saveDocument
{
   [self.document saveToURL:self.document.fileURL
    forSaveOperation:UIDocumentSaveForOverwriting
          completionHandler:^(BOOL success) {
             if (success ) {
                NSLog(@"Save succeeded");
             } else {
                NSLog(@"Error on save");
             }
          }
    ];
}
- (void)handleError:(NSError *)error userInteractionPermitted:(BOOL)userInteractionPermitted
{
   NSLog(@"UIManagedDocument error: %@", error.localizedDescription);
   NSArray* errors = [[error userInfo] objectForKey:NSDetailedErrorsKey];
   if(errors != nil && errors.count > 0) {
      for (NSError *error in errors) {
         NSLog(@"  Error: %@", error.userInfo);
      }
   } else {
      NSLog(@"  %@", error.userInfo);
   }
}
-(void)useDemoDocument {
   if (self.document) {
      self.managedObjectContext = self.document.managedObjectContext;
   } else {
      NSURL *url = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
      url = [url URLByAppendingPathComponent:@"Demo Document"];
      MyUIManagedDocument *document = [[MyUIManagedDocument alloc] initWithFileURL:url];
      NSLog(@"%@ using document %@",self,url);
      if (![[NSFileManager defaultManager] fileExistsAtPath:[url path]]) {
         //create it
         [document saveToURL:url
            forSaveOperation:UIDocumentSaveForCreating
           completionHandler:^(BOOL success) {
              if (success) {
                 self.managedObjectContext = document.managedObjectContext;
                 [self refresh]; // from flickr
              }
           }];
      } else if (document.documentState == UIDocumentStateClosed) {
         //open it
         [document openWithCompletionHandler:^(BOOL success) {
            self.managedObjectContext = document.managedObjectContext;
         }];
      } else {
         //try to use it
         self.managedObjectContext = document.managedObjectContext;
      }
      self.document = document;
      //NSLog(@"%@ context is %@",self,self.managedObjectContext);
   }
}
-(IBAction)refresh
{
   [self.refreshControl beginRefreshing];
   UIApplication *myApplication = [UIApplication sharedApplication];
   dispatch_queue_t fetchQ = dispatch_queue_create("Flickr Fetch", NULL);
   dispatch_async(fetchQ, ^{
      myApplication.networkActivityIndicatorVisible = YES;
      NSArray *photos = [FlickrFetcher latestGeoreferencedPhotos];
      NSLog(@"%@ performing flickrfetch",self);
      //Put the photos in the database
      [self.managedObjectContext performBlock:^{
         myApplication.networkActivityIndicatorVisible = NO;
         for (NSDictionary *photo in photos) {
            [Photo photoWithFlickrInfo:photo inManagedObjectConext:self.managedObjectContext];
         }
         dispatch_async(dispatch_get_main_queue(), ^{
            [self.refreshControl endRefreshing];
         });
      }];
   });
}
-(void)viewDidLoad
{
   [super viewDidLoad];
   self.debug = YES;
   [self.refreshControl addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
}
@end
