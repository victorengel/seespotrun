//
//  Tag+Create.m
//  cspot
//
//  Created by Victor Engel on 4/8/13.
//  Copyright (c) 2013 Victor Engel. All rights reserved.
//

#import "Tag+Create.h"

@implementation Tag (Create)

+(NSSet *)tagWithTag:(NSString *)tagID
inManagedObjectContext:(NSManagedObjectContext *)context
{
   //NSLog(@"%@ context is %@",self,context);
   NSSet *tag = nil;
   if (tagID.length) {
      NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Tag"];
      request.sortDescriptors = @[[NSSortDescriptor
                                   sortDescriptorWithKey:@"tagID"
                                   ascending:YES
                                   selector:@selector(localizedCaseInsensitiveCompare:)]];
      request.predicate = [NSPredicate predicateWithFormat:@"tagID = %@", tagID];
      NSError *error;
      NSArray *matches = [context executeFetchRequest:request error:&error];
      
      if (!matches || [matches count] > 1) {
         // handle error
         NSLog(@"%@ error",self);
      } else if (![matches count]) {
         Tag *tag = [NSEntityDescription insertNewObjectForEntityForName:@"Tag"
                                                      inManagedObjectContext:context];
         tag.tagID = tagID;
      } else {
         tag = matches[0];
      }
   }
   return tag;
}
@end
