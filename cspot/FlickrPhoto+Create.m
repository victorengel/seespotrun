//
//  FlickrPhoto+Create.m
//  cspot
//
//  Created by Victor Engel on 4/11/13.
//  Copyright (c) 2013 Victor Engel. All rights reserved.
//

#import "FlickrPhoto+Create.h"
#import "FlickrFetcher.h"

@implementation FlickrPhoto (Create)

+(FlickrPhoto *)flickrPhotoWithPhotoID:(NSString *)photoID
                inManagedObjectContext:(NSManagedObjectContext *)context
                                 width:(NSNumber *)width
                                height:(NSNumber *)height
                                   url:(NSString *)url
                                source:(NSString *)source
                                 label:(NSString *)label
{
   FlickrPhoto *flickrPhoto = nil;
   // All passed fields are required
   NSLog(@"flickrPhotoWithPhotoID %@ %@ %@ %@ %@ %@",photoID, width, height, url, source, label);
   if (photoID && width && height && url && source) {
      NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"FlickrPhoto"];
      NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"photoID = %@", photoID];
      NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"width = %@",width];
      NSPredicate *predicate3 = [NSPredicate predicateWithFormat:@"height = %@",height];
      NSArray *array = [NSArray arrayWithObjects:predicate1, predicate2, predicate3, nil];
      NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:array];
      request.predicate = predicate;
      // Add rest of predicate, above
      NSError *error;
      NSArray *matches = [context executeFetchRequest:request error:&error];
      if (!matches || [matches count] > 1) {
         // We have an error
      } else if (![matches count]) {
         // no error; record doesn't exist, so create it
         flickrPhoto = [NSEntityDescription insertNewObjectForEntityForName:@"FlickrPhoto" inManagedObjectContext:context];
         flickrPhoto.photoID = photoID;
         flickrPhoto.width = width;
         flickrPhoto.height = height;
         flickrPhoto.largeDimension = (width > height) ? width : height;
         flickrPhoto.url = url;
         flickrPhoto.source = source;
         flickrPhoto.label = label;
         flickrPhoto.dateCached = [NSDate date];
         flickrPhoto.dateViewed = [NSDate date];
         NSLog(@"Inserted new row into flickrPhoto for photo %@ size %@",photoID,label);
      } else {
         // record exists. Return it.
         flickrPhoto = matches[0];
         flickrPhoto.dateViewed = [NSDate date];
         //photo.photoTitle = [photoDictionary[FLICKR_PHOTO_TITLE] description];
      }
   }
   return flickrPhoto;
}

+(FlickrPhoto *)flickrPhotoWithPhotoID:(NSString *)photoID
                inManagedObjectContext:(NSManagedObjectContext *)context
                    largestSmallerThan:(int)sizeLimit
{
   FlickrPhoto *flickrPhoto = nil;
   NSLog(@"flickerPhotoWithPhotoID %@ %d",photoID, sizeLimit);
   if (photoID && (sizeLimit > 0)) {
      //Find the largest picture with height and with smaller than sizeLimit
      NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"FlickrPhoto"];
      NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"photoID = %@", photoID];
      NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"width <= %d",sizeLimit];
      NSPredicate *predicate3 = [NSPredicate predicateWithFormat:@"height <= %d",sizeLimit];
      NSArray *array = [NSArray arrayWithObjects:predicate1, predicate2, predicate3, nil];
      NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:array];
      request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"largeDimension" ascending:NO]];
      request.predicate = predicate;
      NSError *error;
      NSArray *matches = [context executeFetchRequest:request error:&error];
      if (!matches) {
         //Error
      } else if ([matches count]) {
         // Found a record - return it.
         flickrPhoto = matches[0];
         NSLog(@"Found a %@ picture",flickrPhoto.label);
      } else {
         // No records found. Fetch the available sizes for this photo and populate FlickrPhoto.
         NSArray *availableSizes = [FlickrFetcher availableSizesForPhoto:photoID];
         int bestSoFar = 0;
         for (NSDictionary *availableSize in availableSizes) {
            FlickrPhoto *testFlickrPhoto = [NSEntityDescription insertNewObjectForEntityForName:@"FlickrPhoto" inManagedObjectContext:context];
            testFlickrPhoto.photoID = photoID;
            //To get an NSNumber from a dictionary:
            //NSNumber *message_id = [NSNumber numberWithInt:[[dictionary valueForKey:@"message_id"] intValue]];
            int width = [availableSize[FLICKRPHOTO_WIDTH] intValue];
            int height = [availableSize[FLICKRPHOTO_HEIGHT] intValue];
            int largeDimension;
            testFlickrPhoto.width = [NSNumber numberWithInt:[availableSize[FLICKRPHOTO_WIDTH] intValue]];
            testFlickrPhoto.height = [NSNumber numberWithInt:[availableSize[FLICKRPHOTO_HEIGHT] intValue]];
            testFlickrPhoto.label = availableSize[FLICKRPHOTO_LABEL];
            NSLog(@"Checking if %@ photo meets criteria",testFlickrPhoto.label);
            NSLog(@"Dimensions are %@ by %@",testFlickrPhoto.width,testFlickrPhoto.height);
            testFlickrPhoto.source = availableSize[FLICKRPHOTO_SOURCE];
            testFlickrPhoto.url = availableSize[FLICKRPHOTO_URL];
            if (width > height) {
               largeDimension = width;
            } else {
               largeDimension = height;
            }
            NSLog(@"Largest dimension is %d",largeDimension);
            testFlickrPhoto.largeDimension = @(largeDimension);
            // Now check if the new record matches the selection criteria.
            if (largeDimension <= sizeLimit) {
               NSLog(@"Dimensions acceptable");
               if (!flickrPhoto) {
                  flickrPhoto = testFlickrPhoto;
                  NSLog(@"Found a photo that matches criteria: %@",testFlickrPhoto);
               } else {
                  // already have a candidate -- check if new record is a better candidate
                  if (largeDimension > bestSoFar) {
                     flickrPhoto = testFlickrPhoto;
                     bestSoFar = largeDimension;
                     NSLog(@"Found a better photo that matches criteria: %@",flickrPhoto);
                  } else {
                     NSLog(@"New photo is not better than best so far, which is: %@",flickrPhoto);
                  }
               }
            }
         }
      }
   }
   NSLog(@"Returning %@",flickrPhoto);
   return flickrPhoto;
}

@end
