//
//  Photographer+Create.h
//  cspot
//
//  Created by Victor Engel on 4/3/13.
//  Copyright (c) 2013 Victor Engel. All rights reserved.
//

#import "Photographer.h"

@interface Photographer (Create)

+(Photographer *)photographerWithName:(NSString *)name
               inManagedObjectContext:(NSManagedObjectContext *)context;
                                      
@end
