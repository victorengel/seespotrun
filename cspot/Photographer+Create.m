//
//  Photographer+Create.m
//  cspot
//
//  Created by Victor Engel on 4/3/13.
//  Copyright (c) 2013 Victor Engel. All rights reserved.
//

#import "Photographer+Create.h"

@implementation Photographer (Create)

+(Photographer *)photographerWithName:(NSString *)name
               inManagedObjectContext:(NSManagedObjectContext *)context
{
   //NSLog(@"%@ context is %@",self,context);
   Photographer *photographer = nil;
   if (name.length) {
      NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Photographer"];
      request.sortDescriptors = @[[NSSortDescriptor
                                   sortDescriptorWithKey:@"name"
                                   ascending:YES
                                   selector:@selector(localizedCaseInsensitiveCompare:)]];
      request.predicate = [NSPredicate predicateWithFormat:@"name = %@", name];
      NSError *error;
      NSArray *matches = [context executeFetchRequest:request error:&error];
      
      if (!matches || [matches count] > 1) {
         // handle error
         NSLog(@"%@ error",self);
      } else if (![matches count]) {
         photographer = [NSEntityDescription insertNewObjectForEntityForName:@"Photographer"
                                                      inManagedObjectContext:context];
         photographer.name = name;
         NSLog(@"%@ Inserted %@",self,name);
      } else {
         photographer = matches[0];
         NSLog(@"%@ Already exists: %@",self,name);
         NSLog(@"Has thumbnail? %@",photographer.thumbnail ? @"YES" : @"NO");
      }
   }
   return photographer;
}

@end
