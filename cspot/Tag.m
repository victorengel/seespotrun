//
//  Tag.m
//  cspot
//
//  Created by Victor Engel on 4/11/13.
//  Copyright (c) 2013 Victor Engel. All rights reserved.
//

#import "Tag.h"
#import "Photo.h"


@implementation Tag

@dynamic tagID;
@dynamic photos;

@end
