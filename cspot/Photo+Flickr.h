//
//  Photo+Flickr.h
//  cspot
//
//  Created by Victor Engel on 4/3/13.
//  Copyright (c) 2013 Victor Engel. All rights reserved.
//

#import "Photo.h"

@interface Photo (Flickr)

+ (Photo *)photoWithFlickrInfo:(NSDictionary *)photoDictionary
         inManagedObjectConext:(NSManagedObjectContext *)context;
// with these two arguments, we will be able to create a managed object with flicker information
// The dictionary is what returns from FlickrFetcher.

@end
