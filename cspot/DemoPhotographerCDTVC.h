//
//  DemoPhotographerCDTVC.h
//  cspot
//
//  Created by Victor Engel on 4/3/13.
//  Copyright (c) 2013 Victor Engel. All rights reserved.
//

#import "PhotographerCDTVC.h"
#import "MyUIManagedDocument.h"

@interface DemoPhotographerCDTVC : PhotographerCDTVC

//@property UIManagedDocument *document;
@property (strong,nonatomic) MyUIManagedDocument *document;

@end
