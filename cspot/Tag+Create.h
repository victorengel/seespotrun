//
//  Tag+Create.h
//  cspot
//
//  Created by Victor Engel on 4/8/13.
//  Copyright (c) 2013 Victor Engel. All rights reserved.
//

#import "Tag.h"

@interface Tag (Create)

+(Tag *)tagWithTag:(NSString *)tag
               inManagedObjectContext:(NSManagedObjectContext *)context;

@end
