//
//  PhotosByPhotographerCDTVC.m
//  cspot
//
//  Created by Victor Engel on 4/4/13.
//  Copyright (c) 2013 Victor Engel. All rights reserved.
//

#import "PhotosByPhotographerCDTVC.h"
#import "Photo.h"
#import "Photographer.h"
#import "FlickrFetcher.h"
#import "FlickrPhoto+Create.h"


@implementation PhotosByPhotographerCDTVC

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
   //User tapped on a photo item.
   NSIndexPath *indexPath = nil;
   
   if ([sender isKindOfClass:[UITableViewCell class]]) {
      indexPath = [self.tableView indexPathForCell:sender];
   }
   
   if (indexPath) {
      if ([segue.identifier isEqualToString:@"setImageURL:"]) {
         Photo *photo = [self.fetchedResultsController objectAtIndexPath:indexPath];
         // Now we have the photo information the table controller knows about.
         // The destination view controller should be the image view controller.
         // Update the thumbnail.
         if ([segue.destinationViewController respondsToSelector:@selector(setImageURL:)])
         {
            // Decide whether to use large or original size
            CGRect screenRect = [[UIScreen mainScreen] bounds];
            CGFloat screenWidth = screenRect.size.width * [[UIScreen mainScreen] scale];
            CGFloat screenHeight = screenRect.size.height * [[UIScreen mainScreen] scale];
            //Flickr large size has 1024 pixels in the maximum dimension. Use 512 in following line to provide lossless 2X zooming.
            //Modify to use new FlickrPhoto class.
            CGFloat largestDimension = screenWidth * 2;
            if (screenHeight * 2 > largestDimension) {
               largestDimension = screenHeight * 2;
            }
            FlickrPhoto *flickrPhoto = [FlickrPhoto flickrPhotoWithPhotoID:photo.photoID inManagedObjectContext:self.photographer.managedObjectContext largestSmallerThan:largestDimension];
            if (flickrPhoto) {
               NSURL *imageURL = [[NSURL alloc] initWithString:flickrPhoto.source];
               if (imageURL) {
                  NSLog(@"#### imageURL = %@",[imageURL absoluteString]);
                  NSLog(@"     source   = %@",flickrPhoto.source);
                  //Set the thumbnail of the Photographer to the image about to be viewed.
                  //The thumbnail will thus be the last image viewed.
                  Photographer *photographer = photo.photographer;
                  if (photo.thumbnail) {
                     [self.photographer.managedObjectContext performBlock:^{
                        photographer.thumbnail = photo.thumbnail;
                        NSLog(@"Does self.photographer.man..context have changes? %@",[self.photographer.managedObjectContext hasChanges] ? @"YES" : @"NO");
                        /*NSError *error;
                         if (![self.photographer.managedObjectContext save:&error]) {
                         NSLog(@"Error saving context is %@",error);
                         } else {
                         NSLog(@"##### saved context successfully after updating thumbnail");
                         }*/
                     }];
                  }
                  //photographer.thumbnailURL = photo.thumbnailURL;
                  
                  //NSLog(@"##### photo.thumbnail %@ copied to photographer.thumbnail %@",photo.thumbnail,photographer.thumbnail);
                  //NSLog(@"Found a photo %@ to pass to the destination view controller %@",photo,segue.destinationViewController);
                  [segue.destinationViewController performSelector:@selector(setTitle:) withObject:photo.photoTitle];
                  NSLog(@"Segueing with imageURL %@",imageURL);
                  [segue.destinationViewController performSelector:@selector(setImageURL:) withObject:imageURL];
               }
            }
         }
      }
   }
}


-(void) setPhotographer:(Photographer *)photographer
{
   NSLog(@"%@ setPhotographer -- has thumbnail? %@",self,photographer.thumbnail ? @"YES" : @"NO");
   _photographer = photographer;
   self.title = photographer.name;
   [self setupFetchedResultsController];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Photo"];
   
   Photo *photo = [self.fetchedResultsController objectAtIndexPath:indexPath];
   
   cell.textLabel.text = photo.photoTitle;
   cell.detailTextLabel.text = photo.photoDescription;
   cell.imageView.image = [[UIImage alloc] initWithData:[self thumbnailForPhoto:photo]];
   
   return cell;
}

-(NSData *)thumbnailForPhoto: (Photo *)photo
{
   // Get the thumbnail for photo and return it. Add it to the database if it is  not already there.
   NSData *thumbnail = [[NSData alloc] init];
   if (photo.thumbnail) {
      thumbnail = photo.thumbnail;
   } else {
      //We don't have a thumbnail, so let's get one.
      //NSArray *availableSizes = [FlickrFetcher availableSizesForPhoto:photo.photoID];

      UIApplication *myApplication = [UIApplication sharedApplication];
      NSURL *thumbnailURL = [[NSURL alloc]initWithString:photo.thumbnailURL];
      dispatch_queue_t downloadQueue = dispatch_queue_create("thumbnail downloader", NULL);
      dispatch_async(downloadQueue, ^{
         myApplication.networkActivityIndicatorVisible = YES;
         NSData *newThumbnail = [[NSData alloc]initWithContentsOfURL:thumbnailURL];
         dispatch_async(dispatch_get_main_queue(), ^{
            photo.thumbnail = newThumbnail;
            myApplication.networkActivityIndicatorVisible = NO;
         });
      });
   }
   return thumbnail;
}

-(void)setupFetchedResultsController
{
   if (self.photographer.managedObjectContext) {
      //NSLog(@"%@ context is %@",self,self.photographer.managedObjectContext);
      NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Photo"];//database table name
      request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"photoTitle" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)]];
      request.predicate = [NSPredicate predicateWithFormat:@"photographer = %@", self.photographer];
      
      self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request
                                                                          managedObjectContext:self.photographer.managedObjectContext
                                                                            sectionNameKeyPath:nil
                                                                                     cacheName:nil];
   } else {
      self.fetchedResultsController = nil;
   }
}


@end
